package com.utrwalanie.projekt;

import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Artist;
import com.utrwalanie.projekt.domain.Song;
import com.utrwalanie.projekt.service.AlbumRepository;
import com.utrwalanie.projekt.service.ArtistRepository;
import com.utrwalanie.projekt.service.SongRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProjektApplicationTests {

	@Autowired
	AlbumRepository albumRepository;
	@Autowired
	ArtistRepository artistRepository;
	@Autowired
	SongRepository songRepository;

	@BeforeEach
	void clear() {
		songRepository.deleteAll();
		albumRepository.deleteAll();
		artistRepository.deleteAll();
	}

	//insert
	@Test
	void createTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song = new Song("Powiedz ile masz lat", (short) 300, album, false);
		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song);

		assertTrue(artistRepository.findById(artist.getId()).isPresent());
		assertEquals(artistRepository.findById(artist.getId()).get(), artist);

		assertTrue(albumRepository.findById(album.getId()).isPresent());
		assertEquals(albumRepository.findById(album.getId()).get(), album);

		assertTrue(songRepository.findById(song.getId()).isPresent());
		assertEquals(songRepository.findById(song.getId()).get(), song);
	}

	@Test
	void incrementationTest() {
		Artist artist1 = new Artist("Ich Troje", "Polska");
		Album album1 = new Album("Intro", Date.valueOf("1996-09-09"), artist1);
		Song song1 = new Song("Kot", (short) 320, album1, false);
		Artist artist2 = new Artist("Urszuli", "Polska");
		Album album2 = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist2);
		Song song2 = new Song("Powiedz ile masz lat", (short) 300, album2, false);

		artistRepository.save(artist1);
		artistRepository.save(artist2);
		albumRepository.save(album1);
		albumRepository.save(album2);
		songRepository.save(song1);
		songRepository.save(song2);

		assertEquals(artist1.getId(), artist2.getId() - 1);
		assertEquals(album1.getId(), album2.getId() - 1);
		assertEquals(song1.getId(), song2.getId() - 1);
	}

	//select
	@Test
	void readTest() {
		Artist artist1 = new Artist("Ich Troje", "Polska");
		Album album1 = new Album("Intro", Date.valueOf("1996-09-09"), artist1);
		Song song1 = new Song("Kot", (short) 320, album1, false);
		Artist artist2 = new Artist("Urszuli", "Polska");
		Album album2 = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist2);
		Song song2 = new Song("Powiedz ile masz lat", (short) 300, album2, false);

		artistRepository.save(artist1);
		artistRepository.save(artist2);
		albumRepository.save(album1);
		albumRepository.save(album2);
		songRepository.save(song1);
		songRepository.save(song2);

		assertEquals(artistRepository.findOneById(artist1.getId()), artist1);
		assertEquals(artistRepository.findOneById(artist2.getId()), artist2);
		assertEquals(songRepository.findOneById(song1.getId()), song1);
		assertEquals(albumRepository.findOneById(album2.getId()), album2);
	}

	@Test
	void updateTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song = new Song("Powiedz ile masz lat", (short) 300, album, false);
		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song);

		Artist a = artistRepository.getOne(artist.getId());
		a.setCountry("Czechy");
		artistRepository.save(a);

		Song s = songRepository.getOne(song.getId());
		s.setName("Lęk");
		songRepository.save(s);

		assertEquals(artistRepository.findOneById(artist.getId()).getCountry(), "Czechy");
		assertEquals(songRepository.findOneById(song.getId()).getName(), "Lęk");

	}

	@Test
	void deleteTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song = new Song("Powiedz ile masz lat", (short) 300, album, false);
		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song);

		assertTrue(songRepository.existsById(song.getId()));
		songRepository.delete(song);
		assertFalse(songRepository.existsById(song.getId()));

	}

	//testy z @Query
	@Test
	void findAllNewerThan() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album1 = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Album album2 = new Album("Udar", Date.valueOf("2001-01-05"), artist);
		Album album3 = new Album("Złota Kolekcja", Date.valueOf("2003-10-10"), artist);

		artistRepository.save(artist);
		albumRepository.save(album1);
		albumRepository.save(album2);
		albumRepository.save(album3);

		List<Album> albums = albumRepository.findAllNewerThan(Date.valueOf("2000-01-01"));
		assertEquals(albums.size(), 2);
	}


	//testy z @NamedQuery
	@Test
	void findAllExplicitTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song1 = new Song("Powiedz ile masz lat", (short) 300, album, false);
		Song song2 = new Song("Czeska biżuteria", (short) 400, album, true);
		Song song3 = new Song("Brazylia mieszka we mnie", (short) 720, album, true);
		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song1);
		songRepository.save(song2);
		songRepository.save(song3);

		int songsExplicit = songRepository.countExplicit();

		assertEquals(songsExplicit, 2);
	}

	@Test
	void findAllShorterThan() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song1 = new Song("Powiedz ile masz lat", (short) 300, album, false);
		Song song2 = new Song("Czeska biżuteria", (short) 400, album, true);
		Song song3 = new Song("Brazylia mieszka we mnie", (short) 720, album, true);
		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song1);
		songRepository.save(song2);
		songRepository.save(song3);

		List<Song> songs = songRepository.findAllShorterThan((short) 520);

		assertEquals(songs.size(), 2);
	}

	@Test
	void findAllFromCountryTest() {
		Artist artist1 = new Artist("Urszula", "Polska");
		Artist artist2 = new Artist("Ich Troje", "Polska");
		Artist artist3 = new Artist("Édith Piaf", "Francja");

		artistRepository.save(artist1);
		artistRepository.save(artist2);
		artistRepository.save(artist3);

		List<Artist> artists = artistRepository.findAllFromCountry("Polska");

		assertEquals(artists.size(), 2);
	}
	//testy z Query Methods
	@Test
	void ArtistfindByNameAndCountryOrderByIdTest() {
		Artist artist1 = new Artist("Urszula", "Polska");
		Artist artist2 = new Artist("Urszula", "Francja");
		Artist artist3 = new Artist("Urszula", "Polska");
		Artist artist4 = new Artist("Ada", "Polska");

		artistRepository.save(artist1);
		artistRepository.save(artist2);
		artistRepository.save(artist3);
		artistRepository.save(artist4);

		List<Artist> artists = artistRepository.findByNameAndCountryOrderById("Urszula", "Polska");

		assertEquals(artists.size(), 2);

	}
	@Test
	void SongfindByLengthBetweenAndNameTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song1 = new Song("Powiedz ile masz lat", (short) 300, album, false);
		Song song2 = new Song("Brazylia mieszka we mnie", (short) 400, album, true);
		Song song3 = new Song("Brazylia mieszka we mnie", (short) 120, album, true);
		Song song4 = new Song("Brazylia mieszka we mnie", (short) 720, album, true);
		Song song5 = new Song("Brazylia mieszka we mnie", (short) 520, album, true);

		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song1);
		songRepository.save(song2);
		songRepository.save(song3);
		songRepository.save(song4);
		songRepository.save(song5);

		List<Song> songs = songRepository.findByLengthBetweenAndName((short) 200, (short) 600, "Brazylia mieszka we mnie");

		assertEquals(songs.size(), 2);
	}
	@Test
	void SongfindByNameAndExplicitTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album = new Album("Urszula 3", Date.valueOf("1987-01-01"), artist);
		Song song1 = new Song("Powiedz ile masz lat", (short) 300, album, false);
		Song song2 = new Song("Brazylia mieszka we mnie", (short) 720, album, false);
		Song song3 = new Song("Brazylia mieszka we mnie", (short) 520, album, true);
		Song song4 = new Song("Brazylia mieszka we mnie", (short) 520, album, true);

		artistRepository.save(artist);
		albumRepository.save(album);
		songRepository.save(song1);
		songRepository.save(song2);
		songRepository.save(song3);
		songRepository.save(song4);

		List<Song> songs = songRepository.findByNameAndExplicit("Brazylia mieszka we mnie", true);

		assertEquals(songs.size(), 2);
	}
	@Test
	void AlbumfindByReleaseBetweenOrderByNameAscTest() {
		Artist artist = new Artist("Urszuli", "Polska");
		Album album1 = new Album("Urszula", Date.valueOf("1987-01-01"), artist);
		Album album2 = new Album("Urszula", Date.valueOf("2004-01-05"), artist);
		Album album3 = new Album("Urszula", Date.valueOf("2005-10-10"), artist);

		artistRepository.save(artist);
		albumRepository.save(album1);
		albumRepository.save(album2);
		albumRepository.save(album3);

		List<Album> albums = albumRepository.findByReleaseAfterAndNameOrderById(Date.valueOf("2002-02-02"), "Urszula");

		assertEquals(albums.size(), 2);

	}
}
