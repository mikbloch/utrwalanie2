package com.utrwalanie.projekt.service;


import com.utrwalanie.projekt.domain.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ArtistRepository extends JpaRepository<Artist, Long> {

    List<Artist> findAllFromCountry(String country);

    <S extends Artist> S save(S Artist);

    void delete(Artist artist);

    Artist findOneById(Long id);

    List<Artist> findByNameAndCountryOrderById(String Name, String Country);

}
