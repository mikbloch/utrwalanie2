package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Transactional
public interface AlbumRepository extends JpaRepository<Album, Long> {

    Album findOneById(Long id);

    void delete(Album album);

    @Query(value = "select a from Album a where a.release > ?1")
    List<Album> findAllNewerThan(Date date);

    List<Album> findByReleaseAfterAndNameOrderById(Date Release, String Name);

}
