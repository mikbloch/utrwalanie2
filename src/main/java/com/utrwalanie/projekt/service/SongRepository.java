package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Artist;
import com.utrwalanie.projekt.domain.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SongRepository extends JpaRepository<Song, Long> {

    Song findOneById(Long id);

    int countExplicit();

    @Query(value = "Select s from Song s where s.length < ?1")
    List<Song> findAllShorterThan(short length);

    List<Song> findByLengthBetweenAndName(short minLength, short maxLength, String name);

    List<Song> findByNameAndExplicit(String Name, boolean Explicit);

}
