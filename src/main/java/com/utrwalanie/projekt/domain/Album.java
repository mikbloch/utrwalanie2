package com.utrwalanie.projekt.domain;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Album {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private Date release;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    Artist artist;
    @OneToMany
    @EqualsAndHashCode.Exclude
    List<Song> songs;

    public Album(String name, Date release, Artist artist) {
        this.name = name;
        this.release = release;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }
}
