package com.utrwalanie.projekt.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@NamedQuery(name="Artist.findAllFromCountry", query = "select a from Artist a where a.country = ?1")
public class Artist {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String country;
    @OneToMany
    @EqualsAndHashCode.Exclude
    List<Album> albums;

    public Artist(String name, String country){
        this.name = name;
        this.country = country;
        this.albums = new ArrayList<Album>();
    }
}
