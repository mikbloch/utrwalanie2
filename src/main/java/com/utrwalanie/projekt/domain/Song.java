package com.utrwalanie.projekt.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter @Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@NamedQuery(name = "Song.countExplicit", query = "select count(s) from Song s where s.explicit = true")
public class Song {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private short length;
    private boolean explicit;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    Album album;

    public Song(String name, short length, Album album, boolean explicit) {
        this.name = name;
        this.length = length;
        this.album = album;
        this.explicit = explicit;
    }
}
