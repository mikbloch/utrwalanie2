create table album (
                       id bigint not null,
                       name varchar(255),
                       release date,
                       artist_id bigint,
                       primary key (id)
);

create table album_songs (
                             album_id bigint not null,
                             songs_id bigint not null
);

create table artist (
                        id bigint not null,
                        country varchar(255),
                        name varchar(255),
                        primary key (id)
);

create table artist_albums (
                               artist_id bigint not null,
                               albums_id bigint not null
);

create table song (
                      id bigint not null,
                      explicit boolean not null,
                      length smallint not null,
                      name varchar(255),
                      album_id bigint,
                      primary key (id)
);

alter table album_songs
    add constraint UK_ad6gms9d256tcj3ep8om9n1pr unique (songs_id);

alter table artist_albums
    add constraint UK_rib3oys16i57ecfmscw7ggeem unique (albums_id);

alter table album add constraint FKmwc4fyyxb6tfi0qba26gcf8s1
    foreign key (artist_id)
        references artist(id);

alter table album_songs
    add constraint FKrkacen1hcenpf1yby2u0q5cr0
        foreign key (songs_id)
            references song(id);

alter table album_songs
    add constraint FK4p67gti7olml2nebymwwgx3uw
        foreign key (album_id)
            references album(id);

alter table artist_albums
    add constraint FKsre84mirhau0r4nnl7sdwqqdy
        foreign key (albums_id)
            references album(id);

alter table artist_albums
    add constraint FKbcvl559apfrlou44wn8x039f4
        foreign key (artist_id)
            references artist(id);

alter table song
    add constraint FKrcjmk41yqj3pl3iyii40niab0
        foreign key (album_id)
            references album(id);